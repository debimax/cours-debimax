

#################### exercice 1  #####################
# ex1  1)
def est_caractere_minuscule(caractere):
    '''Fonction qui détermine si le caratère est un caractère minuscule'''
    if 97<=ord(caractere)<=122:   # conversion  en code ascii
        return True
    else:
        return False

# Il y a de nombreuses variantes
def est_minuscule(chaine):
    for caractere in chaine:
        if not est_caractere_minuscule(caractere):  #  si le caractère n'est pas en minuscule 
            return False  #on s'arrète et on réponse False
    return True

print("Tests pour savoir si les caractères sont en minuscules")
print("texte", est_minuscule("texte"))
print("TEXTE", est_minuscule("TEXTE"))
print("Texte", est_minuscule("Texte"))
print("textE", est_minuscule("TextE"))

# ex1  2) 
def est_caractere_majuscule(caractere):
    '''Fonction qui détermine si le caratère est un caractère majuscule'''
    if 65<=ord(caractere)<=90:   # conversion  en code ascii
        return True
    else:
        return False

# Il y a de nombreuses variantes
def est_majuscule(chaine): 
    for caractere in chaine:
        if not est_caractere_majuscule(caractere):  #  si le caractère n'est pas en majuscule 
            return False  #on s'arrète et on réponse False
    return True

print("Tests pour savoir si les caractères sont en majuscules")
print("texte   ", est_majuscule("texte"))
print("TEXTE   ", est_majuscule("TEXTE"))
print("Texte   ", est_majuscule("Texte"))
print("textE   ", est_majuscule("TextE"))


# ex1 3) 

def convertit_carractere_en_minuscule(caractere):
    return chr(ord(caractere)+32)


def convertit_minuscule(chaine):
    reponse = ''
    for caractere in chaine:
        if est_caractere_majuscule(caractere):
            reponse += convertit_carractere_en_minuscule(caractere)
        else:
            reponse += caractere
    return reponse


print(convertit_minuscule("Pablo Neruda"))


# ex1  4) 


def convertit_carractere_en_majuscule(caractere):
    return chr(ord(caractere)-32)


def convertit_majuscule(chaine):
    reponse = ''
    for caractere in chaine:
        if est_caractere_minuscule(caractere):
            reponse += convertit_carractere_en_majuscule(caractere)
        else:
            reponse += caractere
    return reponse


print(convertit_majuscule("Pablo Neruda")) 



#################### exercice 2  #####################

def est_chiffre(caractere):
    '''Fonction qui détermine si le caratère est un chiffre'''
    if 48<=ord(caractere)<=57:   # conversion  en code ascii
        return True
    else:
        return False



def est_nombre_entier(chaine):
    for caractere in chaine:
        if not est_chiffre(caractere):  #  si le caractère n'est pas un  chiffre
            return False  #on s'arrète et on réponse False
    return True

print("Tests pour savoir si tous les caractères sont des chiffres")
print("1234   ", est_nombre_entier("1234"))
print("o123   ", est_nombre_entier("o123"))
print("+123   ", est_nombre_entier("+123"))
print("0123456789   ", est_nombre_entier("0123456789"))




#################### exercice 3  #####################
def cesar_caractere(caractere,cle):
    '''Fonction qui code un caractère selon  le codage de cesar.'''
    asc2i=ord(caractere)
    if 65<=asc2i<=90-cle :   
        return chr(asc2i+cle)
    elif 90- cle < asc2i<=90 :
        return  chr(asc2i-26+cle)
    else:
        return " "
print(cesar_caractere("Z",3))

