# Cours debimax

##  [Lien pour Ma classe  à la maison](https://eu.bbcollab.com/collab/ui/session/guest/e488a51b10fe4f56801249b4294e13fd)

##  Lien de mes cours sur binder:
- [mybinder](https://mybinder.org/v2/git/https%3A%2F%2Fframagit.org%2Fdebimax%2Fcours-debimax/master)  On peut exécuter le code python, Il faut patienter un peu pour le chargement de la page (1minute  environ).

## HTML CSS

* HTML:
	* Documentation <http://w3schools.com/tags/> 
	* Validateur en ligne : <https://validator.w3.org/#validate_by_upload>
* CSS: 
	* Documentation <http://www.w3schools.com/css/>
	* Validateur en ligne : <https://validator.w3.org/unicorn/?ucn_lang=fr#validate-by-upload+task_conformance>

## Sites institutionnels

* Programme de première NSI:  <https://cache.media.education.gouv.fr/file/SP1-MEN-22-1-2019/26/8/spe633_annexe_1063268.pdf>
* Sujet 0 de l'épreuve de contrôle continue de fin de première :
	* <https://cache.media.eduscol.education.fr/file/Annales_zero_BAC_2021_1e/87/9/S0BAC21-1e-SPE-NSI_1133879.pdf>
	* Sujet 0 qui avait été mis en ligne puis remplacé <https://framagit.org/flaustriat/nsi-1/blob/master/docs/sujet00.pdf>

